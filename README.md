# StreetViewPanoFetcher
Fetches all Street View Panoramas in a radius around a set of starting coordinates, then outputs their PanoIDs along with some image metadata.

This tool is used by [SkyViewFactorTool](https://git.mpib-berlin.mpg.de/metzner/SkyViewFactorTool), a program that fetches all available Google Street View images around a starting point, then calculates all their Sky View Factors using image processing, and finally outputs the average Sky View Factor.
